package com.mtribes.challenge;

import android.widget.Toast;

import androidx.annotation.StringRes;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
public class Toaster {

    private static Toaster toaster = new Toaster();

    private Toast toast;

    private Toaster() {
    }

    public static void showMessage(@StringRes int message) {
        toaster.cancelCurrentToast();
        toaster.makeToast(MTribesChallengeApp.getContext().getString(message));
    }

    private void cancelCurrentToast() {
        if (toast != null) {
            toast.cancel();
        }
    }

    private void makeToast(String message) {
        toast = Toast.makeText(MTribesChallengeApp.getContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

}
