package com.mtribes.challenge;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.mtribes.challenge.di.DaggerAppComponent;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
public class MTribesChallengeApp extends Application implements HasActivityInjector {

    private static MTribesChallengeApp context;

    @Inject
    DispatchingAndroidInjector<Activity> activityDispatchingAndroidInjector;

    @Override
    public void onCreate() {
        super.onCreate();

        context = this;

        DaggerAppComponent.builder().application(this).build().inject(this);
    }

    public static Context getContext() {
        return context;
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityDispatchingAndroidInjector;
    }

}
