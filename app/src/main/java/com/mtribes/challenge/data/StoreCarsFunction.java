package com.mtribes.challenge.data;

import com.mtribes.challenge.data.db.CarInfoDao;
import com.mtribes.challenge.data.db.CarInfoEntity;
import com.mtribes.challenge.data.network.models.PlacemarksRaw;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.functions.Function;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
public class StoreCarsFunction implements Function<PlacemarksRaw, Completable> {

    @Inject
    CarInfoDao carInfoDao;

    @Inject
    public StoreCarsFunction() {
    }

    @Override
    public Completable apply(PlacemarksRaw placemarksRaw) {
        return Flowable.fromIterable(placemarksRaw.getPlacemarks())
                .map(carInfoRaw -> {
                    CarInfoEntity carInfoEntity = new CarInfoEntity();
                    carInfoEntity.setName(carInfoRaw.getName());
                    carInfoEntity.setAddress(carInfoRaw.getAddress());
                    carInfoEntity.setLng(carInfoRaw.getCoordinates().get(0));
                    carInfoEntity.setLat(carInfoRaw.getCoordinates().get(1));
                    return carInfoEntity;
                })
                .toList()
                .doOnSuccess(carInfoEntities -> carInfoDao.insertAll(carInfoEntities))
                .toCompletable();
    }

}
