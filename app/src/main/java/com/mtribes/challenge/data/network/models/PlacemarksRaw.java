package com.mtribes.challenge.data.network.models;

import java.util.List;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
public class PlacemarksRaw {

    private List<CarInfoRaw> placemarks;

    public List<CarInfoRaw> getPlacemarks() {
        return placemarks;
    }

}
