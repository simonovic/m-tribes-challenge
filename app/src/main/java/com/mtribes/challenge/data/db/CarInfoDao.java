package com.mtribes.challenge.data.db;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import io.reactivex.Flowable;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
@Dao
public abstract class CarInfoDao {

    @Query("SELECT * FROM " + CarInfoEntity.TABLE_NAME)
    public abstract Flowable<List<CarInfoEntity>> getAll();

    @Insert
    public abstract void insertAll(List<CarInfoEntity> cars);

}
