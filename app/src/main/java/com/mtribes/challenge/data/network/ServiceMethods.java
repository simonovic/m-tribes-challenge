package com.mtribes.challenge.data.network;

import com.mtribes.challenge.data.network.models.PlacemarksRaw;

import io.reactivex.Single;
import retrofit2.http.GET;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
public interface ServiceMethods {

    @GET("/locations.json")
    Single<PlacemarksRaw> fetchCars();

}
