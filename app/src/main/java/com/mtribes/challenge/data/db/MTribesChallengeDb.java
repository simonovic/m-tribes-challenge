package com.mtribes.challenge.data.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
@Database(entities = {CarInfoEntity.class}, version = 1)
public abstract class MTribesChallengeDb extends RoomDatabase {

    public abstract CarInfoDao carInfoDao();

}
