package com.mtribes.challenge.data;

import com.mtribes.challenge.data.db.CarInfoDao;
import com.mtribes.challenge.data.db.CarInfoEntity;
import com.mtribes.challenge.data.network.ServiceMethods;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
public class CarsRepository {

    @Inject
    CarInfoDao carInfoDao;
    @Inject
    ServiceMethods serviceMethods;
    @Inject
    StoreCarsFunction storeCars;

    @Inject
    public CarsRepository() {
    }

    public Flowable<List<CarInfoEntity>> getCars() {
        return carInfoDao.getAll();
    }

    // assumption is that cars should be fetched and stored only once (if not present in local db)
    public Completable fetchAndStoreCars() {
        return isDbEmpty()
                .flatMapSingle(carInfoEntities -> serviceMethods.fetchCars())
                .flatMapCompletable(storeCars)
                .subscribeOn(Schedulers.io());
    }

    private Flowable<List<CarInfoEntity>> isDbEmpty() {
        return getCars()
                .take(1)
                .filter(carInfoEntities -> carInfoEntities.size() == 0);
    }

}
