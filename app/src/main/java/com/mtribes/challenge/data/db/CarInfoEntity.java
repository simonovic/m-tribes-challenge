package com.mtribes.challenge.data.db;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
@Entity(tableName = CarInfoEntity.TABLE_NAME)
public class CarInfoEntity {

    public static final String TABLE_NAME = "car_info";

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private String address;
    private float lat;
    private float lng;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }
}
