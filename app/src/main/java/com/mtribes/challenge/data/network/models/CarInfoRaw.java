package com.mtribes.challenge.data.network.models;

import java.util.List;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
public class CarInfoRaw {

    private String address;
    private String engineType;
    private String exterior;
    private String interior;
    private String name;
    private String vin;
    private int fuel;
    private List<Float> coordinates;

    public String getAddress() {
        return address;
    }

    public String getEngineType() {
        return engineType;
    }

    public String getExterior() {
        return exterior;
    }

    public String getInterior() {
        return interior;
    }

    public String getName() {
        return name;
    }

    public String getVin() {
        return vin;
    }

    public int getFuel() {
        return fuel;
    }

    public List<Float> getCoordinates() {
        return coordinates;
    }

}
