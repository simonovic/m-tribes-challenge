package com.mtribes.challenge.di;

import com.mtribes.challenge.MTribesChallengeApp;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        AppModule.class,
        UiModule.class,
        DbModule.class,
        NetworkModule.class
})
public interface AppComponent {

    void inject(MTribesChallengeApp application);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(MTribesChallengeApp application);

        AppComponent build();
    }

}
