package com.mtribes.challenge.di;

import com.mtribes.challenge.ui.ViewModelProviderFactory;
import com.mtribes.challenge.ui.cars.CarsFragment;
import com.mtribes.challenge.ui.cars.CarsViewModel;
import com.mtribes.challenge.ui.main.MainActivity;
import com.mtribes.challenge.ui.main.MainViewModel;
import com.mtribes.challenge.ui.map.MapFragment;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import dagger.Binds;
import dagger.MapKey;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.multibindings.IntoMap;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
@Module
public abstract class UiModule {

    @Documented
    @Target({ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @MapKey
    @interface ViewModelKey {
        Class<? extends ViewModel> value();
    }

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelProviderFactory factory);

    @ContributesAndroidInjector()
    abstract MainActivity bindMainActivity();

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel.class)
    abstract ViewModel bindMainViewModel(MainViewModel mainViewModel);

    @ContributesAndroidInjector()
    abstract CarsFragment provideCarsFragment();

    @Binds
    @IntoMap
    @ViewModelKey(CarsViewModel.class)
    abstract ViewModel bindCarsViewModel(CarsViewModel carsViewModel);

    @ContributesAndroidInjector()
    abstract MapFragment provideMapFragment();

}
