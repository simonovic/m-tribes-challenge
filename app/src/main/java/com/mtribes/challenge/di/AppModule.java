package com.mtribes.challenge.di;

import android.app.Application;
import android.content.Context;

import com.mtribes.challenge.MTribesChallengeApp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
@Module
public class AppModule {

    @Provides
    Context provideAppContext(MTribesChallengeApp application) {
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    Application provideApplication(MTribesChallengeApp application) {
        return application;
    }

}
