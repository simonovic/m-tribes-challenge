package com.mtribes.challenge.di;

import android.content.Context;

import com.mtribes.challenge.data.db.CarInfoDao;
import com.mtribes.challenge.data.db.MTribesChallengeDb;

import javax.inject.Singleton;

import androidx.room.Room;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
@Module
public class DbModule {

    @Singleton
    @Provides
    CarInfoDao providePostDao(MTribesChallengeDb database) {
        return database.carInfoDao();
    }

    @Singleton
    @Provides
    MTribesChallengeDb provideDatabase(Context context) {
        return Room.databaseBuilder(context, MTribesChallengeDb.class, "MTribesChallenge").build();
    }

}
