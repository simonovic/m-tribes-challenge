package com.mtribes.challenge.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mtribes.challenge.BR;

import javax.inject.Inject;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import dagger.android.support.DaggerFragment;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
public abstract class BaseFragment<BindingT extends ViewDataBinding, ViewModelT extends ViewModel> extends DaggerFragment {

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private BindingT binding;
    private ViewModelT viewModel;
    private View view;

    @SuppressWarnings("ConstantConditions")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (this.view == null) {
            binding = DataBindingUtil.inflate(inflater, this.provideLayoutId(), container, false);
        } else {
            binding = DataBindingUtil.getBinding(this.view);
        }

        binding.setLifecycleOwner(this);
        viewModel = ViewModelProviders.of(this, this.viewModelFactory).get(this.provideViewModelClass());
        binding.setVariable(BR.vm, viewModel);
        view = this.binding.getRoot();
        return view;
    }

    public BindingT getBinding() {
        return binding;
    }

    protected ViewModelT getViewModel() {
        return viewModel;
    }

    @LayoutRes
    protected abstract int provideLayoutId();

    protected abstract Class<ViewModelT> provideViewModelClass();

}
