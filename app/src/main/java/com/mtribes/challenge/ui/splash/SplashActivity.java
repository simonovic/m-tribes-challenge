package com.mtribes.challenge.ui.splash;

import android.os.Bundle;

import com.mtribes.challenge.ui.main.MainActivity;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        startActivity(MainActivity.createIntent());
    }
}
