package com.mtribes.challenge.ui;

import com.mtribes.challenge.MTribesChallengeApp;
import com.mtribes.challenge.R;
import com.mtribes.challenge.ui.cars.CarsFragment;
import com.mtribes.challenge.ui.map.MapFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
public class CarsPagerAdapter extends FragmentPagerAdapter {

    private static final int COUNT = 2;
    private static final int CARS_POSITION = 0;

    public CarsPagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case CARS_POSITION:
                return CarsFragment.newInstance();
            default:
                return MapFragment.newInstance();
        }
    }

    @Override
    public int getCount() {
        return COUNT;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case CARS_POSITION:
                return MTribesChallengeApp.getContext().getString(R.string.tab_title_list);
            default:
                return MTribesChallengeApp.getContext().getString(R.string.tab_title_map);
        }
    }

}
