package com.mtribes.challenge.ui.map;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mtribes.challenge.R;
import com.mtribes.challenge.Toaster;
import com.mtribes.challenge.databinding.FragmentMapBinding;
import com.mtribes.challenge.ui.BaseFragment;
import com.mtribes.challenge.ui.cars.CarInfoView;
import com.mtribes.challenge.ui.cars.CarsViewModel;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
public class MapFragment extends BaseFragment<FragmentMapBinding, CarsViewModel> implements
        OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMyLocationButtonClickListener {

    private static final LatLng HAMBURG = new LatLng(53.5847, 10.0454);

    private GoogleMap googleMap;
    private boolean showMarkers;
    private List<Marker> carMarkers;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public static MapFragment newInstance() {
        return new MapFragment();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (getFragmentManager() != null) {
            SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
            if (mapFragment != null) {
                mapFragment.getMapAsync(this);
            }
        }
        observeActions();
    }

    public void onDestroy() {
        super.onDestroy();
        this.compositeDisposable.dispose();
    }

    @Override
    protected int provideLayoutId() {
        return R.layout.fragment_map;
    }

    @Override
    protected Class<CarsViewModel> provideViewModelClass() {
        return CarsViewModel.class;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setOnMarkerClickListener(this);
        this.googleMap.setOnMyLocationButtonClickListener(this);
        this.googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(HAMBURG, 10));
        checkLocationPermission();
        addCarMarkers();
    }

    @Override
    public boolean onMarkerClick(Marker clickedCarMarker) {
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(clickedCarMarker.getPosition()));
        if (showMarkers) {
            clickedCarMarker.hideInfoWindow();
        } else {
            clickedCarMarker.showInfoWindow();
        }
        for (Marker carMarker : carMarkers) {
            if (!carMarker.getId().equals(clickedCarMarker.getId())) {
                carMarker.setVisible(showMarkers);
            }
        }
        showMarkers = !showMarkers;

        return true;
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    private void observeActions() {
        getViewModel().getAction().observe(this, action -> {
            switch (action) {
                case CARS_READY:
                    addCarMarkers();
                    break;
                case ERROR:
                    Toaster.showMessage(R.string.error_showing_cars);
                    break;
            }
        });
    }

    private void addCarMarkers() {
        carMarkers = new ArrayList<>();
        for (CarInfoView car : getViewModel().getCars()) {
            carMarkers.add(googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(car.getLatitude(), car.getLongitude()))
                    .title(car.getName())));
        }
    }

    @SuppressLint("MissingPermission")
    private void checkLocationPermission() {
        compositeDisposable.add(
                new RxPermissions(this)
                        .request(Manifest.permission.ACCESS_FINE_LOCATION)
                        .subscribe(isGranted -> {
                                    googleMap.setMyLocationEnabled(isGranted);
                                    googleMap.getUiSettings().setMyLocationButtonEnabled(isGranted);
                                    if (isGranted) {
                                        enableLocation();
                                    } else {
                                        Toaster.showMessage(R.string.enable_permissions);
                                    }
                                },
                                error -> Toaster.showMessage(R.string.error_enabling_location)));
    }

    @SuppressWarnings("ConstantConditions")
    private void enableLocation() {
        LocationManager lm = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        if (lm != null && !lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            new AlertDialog.Builder(getContext())
                    .setTitle("Enable Location")
                    .setMessage("Go to Settings to enable location?")
                    .setNegativeButton("Cancel", (dialog, which) -> {
                    })
                    .setPositiveButton("Ok", (dialog, which) ->
                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))).show();
        }
    }

}
