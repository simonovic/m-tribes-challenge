package com.mtribes.challenge.ui;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
public class BaseViewModel<ActionCodeT> extends ViewModel {

    protected final SingleLiveEvent<ActionCodeT> action = new SingleLiveEvent<>();

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.dispose();
    }

    public LiveData<ActionCodeT> getAction() {
        return action;
    }

    protected void setAction(@NonNull ActionCodeT code) {
        action.postValue(code);
    }

    protected void addDisposable(Disposable disposable) {
        compositeDisposable.add(disposable);
    }

}
