package com.mtribes.challenge.ui.cars.item;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mtribes.challenge.BR;
import com.mtribes.challenge.R;
import com.mtribes.challenge.ui.cars.CarInfoView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
public class CarAdapter extends RecyclerView.Adapter<CarAdapter.CarViewHolder> {

    private List<CarInfoView> cars;

    @Inject
    public CarAdapter() {
        this.cars = new ArrayList<>();
    }

    @NonNull
    @Override
    public CarViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CarViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), viewType, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CarViewHolder holder, int position) {
        holder.bindViewModel(new CarViewModel(cars.get(position)));
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.item_car;
    }

    @Override
    public int getItemCount() {
        return cars.size();
    }

    public void setItems(List<CarInfoView> cars) {
        if (cars == null) {
            this.cars = new ArrayList<>();
        } else {
            this.cars = cars;
        }
        notifyDataSetChanged();
    }

    class CarViewHolder extends RecyclerView.ViewHolder {

        private ViewDataBinding binding;

        CarViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bindViewModel(ViewModel viewModel) {
            this.binding.setVariable(BR.vm, viewModel);
            this.binding.executePendingBindings();
        }

    }

}
