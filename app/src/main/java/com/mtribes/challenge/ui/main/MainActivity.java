package com.mtribes.challenge.ui.main;

import android.content.Intent;
import android.os.Bundle;

import com.mtribes.challenge.MTribesChallengeApp;
import com.mtribes.challenge.R;
import com.mtribes.challenge.Toaster;
import com.mtribes.challenge.databinding.ActivityMainBinding;
import com.mtribes.challenge.ui.CarsPagerAdapter;

import javax.inject.Inject;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
public class MainActivity extends DaggerAppCompatActivity {

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private MainViewModel viewModel;

    public static Intent createIntent() {
        return new Intent(MTribesChallengeApp.getContext(), MainActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        observeActions();
    }

    private void init() {
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setLifecycleOwner(this);
        viewModel = ViewModelProviders.of(this, this.viewModelFactory).get(MainViewModel.class);
        binding.setVm(viewModel);
        binding.viewPager.setAdapter(new CarsPagerAdapter(getSupportFragmentManager()));
        binding.tabLayout.setupWithViewPager(binding.viewPager);
    }

    private void observeActions() {
        viewModel.getAction().observe(this, action -> {
            switch (action) {
                case ERROR_FETCHING_CARS:
                    Toaster.showMessage(R.string.error_fetching_cars);
                    break;
            }
        });
    }

}
