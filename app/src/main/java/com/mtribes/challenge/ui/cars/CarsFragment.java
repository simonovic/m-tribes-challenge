package com.mtribes.challenge.ui.cars;

import android.os.Bundle;
import android.view.View;

import com.mtribes.challenge.R;
import com.mtribes.challenge.Toaster;
import com.mtribes.challenge.databinding.FragmentCarsBinding;
import com.mtribes.challenge.ui.BaseFragment;
import com.mtribes.challenge.ui.cars.item.CarAdapter;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
public class CarsFragment extends BaseFragment<FragmentCarsBinding, CarsViewModel> {

    @Inject
    CarAdapter adapter;

    public static CarsFragment newInstance() {
        return new CarsFragment();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (getBinding().recyclerView.getAdapter() == null) {
            adapter.setItems(getViewModel().getCars());
            getBinding().recyclerView.setAdapter(adapter);
        }
        observeActions();
    }

    @Override
    protected int provideLayoutId() {
        return R.layout.fragment_cars;
    }

    @Override
    protected Class<CarsViewModel> provideViewModelClass() {
        return CarsViewModel.class;
    }

    private void observeActions() {
        getViewModel().getAction().observe(this, action -> {
            switch (action) {
                case CARS_READY:
                    adapter.setItems(getViewModel().getCars());
                    break;
                case ERROR:
                    Toaster.showMessage(R.string.error_showing_cars);
                    break;
            }
        });
    }

}
