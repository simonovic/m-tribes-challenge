package com.mtribes.challenge.ui.cars.item;

import com.mtribes.challenge.ui.cars.CarInfoView;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
public class CarViewModel extends ViewModel {

    private final MutableLiveData<CarInfoView> carInfoLive = new MutableLiveData<>();

    public CarViewModel(CarInfoView carInfo) {
        this.carInfoLive.setValue(carInfo);
    }

    public LiveData<CarInfoView> getCarInfoLive() {
        return carInfoLive;
    }

}
