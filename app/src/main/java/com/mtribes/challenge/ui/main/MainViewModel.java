package com.mtribes.challenge.ui.main;

import android.util.Log;

import com.mtribes.challenge.data.CarsRepository;
import com.mtribes.challenge.ui.BaseViewModel;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
public class MainViewModel extends BaseViewModel<MainViewModel.MainAction> {

    private static final String TAG = MainViewModel.class.getSimpleName();

    public enum MainAction {
        ERROR_FETCHING_CARS
    }

    private CarsRepository carsRepository;

    @Inject
    MainViewModel(CarsRepository carsRepository) {
        this.carsRepository = carsRepository;
        fetchCars();
    }

    private void fetchCars() {
        addDisposable(carsRepository.fetchAndStoreCars()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                        },
                        error -> {
                            Log.e(TAG, error.getMessage());
                            setAction(MainAction.ERROR_FETCHING_CARS);
                        }));
    }

}
