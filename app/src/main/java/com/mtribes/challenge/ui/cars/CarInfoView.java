package com.mtribes.challenge.ui.cars;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
public class CarInfoView {

    private String name;
    private String address;
    private float latitude;
    private float longitude;

    public String getName() {
        return name;
    }

    public CarInfoView setName(String name) {
        this.name = name;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public CarInfoView setAddress(String address) {
        this.address = address;
        return this;
    }

    public float getLatitude() {
        return latitude;
    }

    public CarInfoView setLatitude(float latitude) {
        this.latitude = latitude;
        return this;
    }

    public float getLongitude() {
        return longitude;
    }

    public CarInfoView setLongitude(float longitude) {
        this.longitude = longitude;
        return this;
    }
}
