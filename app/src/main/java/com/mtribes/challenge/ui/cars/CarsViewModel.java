package com.mtribes.challenge.ui.cars;

import android.util.Log;

import com.mtribes.challenge.data.CarsRepository;
import com.mtribes.challenge.data.db.CarInfoEntity;
import com.mtribes.challenge.ui.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
public class CarsViewModel extends BaseViewModel<CarsViewModel.CarsActionCode> {

    private static final String TAG = CarsViewModel.class.getSimpleName();

    public enum CarsActionCode {
        CARS_READY,
        ERROR
    }

    private final MutableLiveData<Boolean> isLoadingLive = new MutableLiveData<>();

    private CarsRepository carsRepository;
    private List<CarInfoView> carsLive;

    @Inject
    public CarsViewModel(CarsRepository carsRepository) {
        this.carsRepository = carsRepository;
        this.isLoadingLive.setValue(true);
        this.carsLive = new ArrayList<>();
        retrieveCars();
    }

    private void retrieveCars() {
        addDisposable(carsRepository.getCars()
                .subscribe(cars -> {
                            carsLive = new ArrayList<>();
                            for (CarInfoEntity carInfoEntity : cars) {
                                carsLive.add(new CarInfoView()
                                        .setName(carInfoEntity.getName())
                                        .setAddress(carInfoEntity.getAddress())
                                        .setLatitude(carInfoEntity.getLat())
                                        .setLongitude(carInfoEntity.getLng()));
                            }
                            setAction(CarsActionCode.CARS_READY);
                            if (cars.size() > 0) {
                                isLoadingLive.postValue(false);
                            }
                        },
                        error -> {
                            Log.e(TAG, error.getMessage());
                            setAction(CarsActionCode.ERROR);
                        }));
    }

    public LiveData<Boolean> getIsLoadingLive() {
        return isLoadingLive;
    }

    public List<CarInfoView> getCars() {
        return carsLive;
    }

}
