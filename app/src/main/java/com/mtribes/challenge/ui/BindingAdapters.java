package com.mtribes.challenge.ui;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;

/**
 * Created by Stefan Simonovic <stefan.simon93@gmail.com>
 */
public class BindingAdapters {

    @BindingAdapter(value = {"visibility", "setInvisible"}, requireAll = false)
    public static void bindVisibility(@NonNull View view, @Nullable Boolean visibility, @Nullable Boolean setInvisible) {
        view.setVisibility(visibility != null && visibility ? View.VISIBLE : setInvisible != null && setInvisible ? View.INVISIBLE : View.GONE);
    }

}
